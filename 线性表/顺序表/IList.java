package com.mj;

public interface IList<T>{
	
	 /*
	  *   获取线性表长度
	  */
     public int length();
     
	 /*
	  *   获取表中下标为i的元素值
	  */
     public T getData(int i);
     
	 /*
	  *   查找索引
	  */
     public int search(T data);
     
	 /*
	  *   在位序index处插入一个元素
	  */
     public void insert(T data,int index);
     
	 /*
	  *   在表的末尾插入元素
	  */
     public void addElement(T data);   
     
	 /*
	  *   删除表中指定索引处的元素
	  */
     public void delete(int index);   
     
	 /*
	  *   判断表是否为空
	  */
     public boolean isEmpty();
     
	 /*
	  *   清空线性表
	  */
     public void clear();
}
